#~ MAKEFILE DEMO UTILISATION DE CUNIT


UNAME := $(shell uname)

ifeq ($(UNAME), Linux)
 GCOVR_CMD = gcovr
 FF_CMD = firefox
endif
ifeq ($(UNAME), Darwin)
 GCOVR_CMD = python gcovr
 FF_CMD = /Applications/Firefox.app/Contents/MacOS/firefox
endif
ifeq ($(UNAME), CYGWIN_NT-6.1)
 GCOVR_CMD = gcovr
 FF_CMD = "/cygdrive/c/Program Files (x86)/Mozilla Firefox/firefox.exe"
endif

LIB_NAME= lib_cu.a
LIB_SRC := CUnit/Sources/Framework/*.c CUnit/Sources/Automated/*.c CUnit/Sources/Basic/*.c
LIB_INC = -ICUnit/Headers
LIB_OBJ := *.o

SRC=Sources/
INC=Includes/
TST=Tests/
CFLAGS = -c -Wall -Wextra
#~~-Werror
GCOV_FLAGS = -fprofile-arcs -ftest-coverage -O0
#~ GCOV_FLAGS = -fprofile-arcs -ftest-coverage -fPIC -O0

NAME = CUnit.test

CC = @gcc

$(LIB_NAME):
	$(CC) $(CFLAGS) $(LIB_INC) $(LIB_SRC)
	@ar rc $(LIB_NAME) $(LIB_OBJ)
	@ranlib $(LIB_NAME)


basic: $(LIB_NAME)
	$(CC) $(TST)exemple1.c $(LIB_INC) lib_cu.a -o exemple1.exe
	./exemple1.exe

test_max: $(LIB_NAME)
	$(CC) $(TST)test_max.c $(SRC)max.c -I$(INC) $(LIB_INC) lib_cu.a -o exemple_2.exe
	./exemple_2.exe

test_func:$(LIB_NAME)
	$(CC) $(TST)run_test.c $(TST)test_func.c $(SRC)func.c -I$(INC)  $(LIB_INC) lib_cu.a -o exemple_3.exe
	./exemple_3.exe

auto: $(LIB_NAME)
	$(CC) $(TST)exemple_automated.c -ICUnit/Headers  $(LIB_INC) lib_cu.a -o exemple_automated.exe
	./exemple_automated.exe
#~ CUnit/Sources/Console/*.c \
#~ CUnit/Sources/Curses/*.c

auto_gcovr: $(LIB_NAME)
	$(CC) $(GCOV_FLAGS) $(TST)run_test.c $(TST)test_func.c $(SRC)func.c	\
-I$(INC)  $(LIB_INC) lib_cu.a -o $(NAME).exe
	./$(NAME).exe
	gcovr -r . --exclude '.*CUnit.*'
	gcovr -r . --exclude '.*CUnit.*' --html --html-details -o $(NAME).html

#~  --exclude-directories=


#~ 	gcc -fprofile-arcs -ftest-coverage -fPIC -O0 $(SRC_SIMPLE) -o $(NAME_SIMPLE)
#~ 	./$(NAME_SIMPLE)
#~ 	gcovr -r .
#~ 	gcovr -r . --html --html-details -o $(NAME_SIMPLE).html



clean:
	@/bin/rm -f *.o
	@/bin/rm -f *.gcda
	@/bin/rm -f *.gcno
	@/bin/rm -f temp.txt
	@/bin/rm -f exemple_automated.exe exemple1.exe

fclean: clean
	@/bin/rm -f *.xml
	@/bin/rm -f *.html
	@/bin/rm -f *.exe

ff:
	$(FF_CMD) *.xml
	$(FF_CMD) *.html


.PHONY: ff clean auto_gcovr auto test_func test_max basic
